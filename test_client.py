import json
import websocket
websocket.enableTrace(True)

from message_ids import *

class Client(object):
    
    def __init__(self, host):
        self.ws = websocket.create_connection(host)
    
    def listen(self):
        result = self.ws.recv_data()
        message = json.loads(result[1])
        action = message['action']
        if action == ACKNOWLEDGE:
            msg_id = message['msg_id']
            if msg_id == NEW_CONNECTION:
                print 'You have connected to {0}.'.format(message['location'])
            elif msg_id == WHO_AM_I:
                print message['data']
            elif msg_id == JUMP_REQUEST:
                pass
            elif msg_id == LIST_LOCATIONS:
                print 'List of locations: {0}.'.format(', '.join(message['locations']))
            elif msg_id == NODE_INFO:
                print message['data']
        elif action == MSG_SYSTEM:
            print '{0}: {1}'.format(message['client_id'], message['message'])
    
    def s_who_am_i(self):
        message = {
            'action': WHO_AM_I,
        }
        self.send_message(message)
    
    def s_jump_request(self, location):
        message = {
            'action': JUMP_REQUEST,
            'location': location,
        }
        self.send_message(message)
    
    def s_list_locations(self):
        message = {
            'action': LIST_LOCATIONS,
        }
        self.send_message(message)
    
    def s_msg_system(self, string):
        message = {
            'action': MSG_SYSTEM,
            'message': string,
        }
        self.send_message(message)
    
    def s_node_info(self):
        message = {
            'action': NODE_INFO,
        }
        self.send_message(message)
    
    def send_message(self, message):
        message = json.dumps(message)
        self.ws.send(message)
        
    def close(self):
        self.ws.close()

if __name__ == "__main__":
    websocket.enableTrace(True)
    client = Client("ws://localhost:22001/")
    
    try:
        while True:
            cmd = raw_input("Command (blank to recv): ")
            if not cmd:
                try:
                    client.listen()
                except KeyboardInterrupt:
                    pass
            args = cmd.split(' ')
            if args[0] == 'whoami':
                client.s_who_am_i()
            elif args[0] == 'jump':
                if len(args) == 2:
                    location = args[1]
                else:
                    location = 'default'
                client.s_jump_request(location)
            elif args[0] == 'list':
                if args[1] == 'locations':
                    client.s_list_locations()
            elif args[0] == 'say':
                client.s_msg_system(' '.join(args[1:]))
            elif args[0] == 'info':
                client.s_node_info()
    
    except KeyboardInterrupt:
        client.close()

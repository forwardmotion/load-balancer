from gevent import monkey
monkey.patch_all()

import json
import gevent
import websocket
websocket.enableTrace(True)


from MessageManager import MessageManager
from message_ids import *

COORDINATOR = 'ws://127.0.0.1:22002/' # 22002 - Coordinator Listening Port for nodes
DEFAULT_NODE = 'default' # Port range 22100 - 24000


class Coordinator(object):
    "This is the WebSocket client that connects to different nodes."
    
    def __init__(self, host):
        self.host = host
        self.ws = None
        self.mq = None
        
        self.nodes = {}
        self.destinations = {} # location: node_id
        
        # This could be an external resource, db, etc.
        self.locations = {
            'default': {
                'name': 'login',
            },
            '1': {
                'name': 'Area 1',
            },
            '2': {
                'name': 'Area 2',
            },
        }
        
        self.start_services()
    
    def start_services(self):
        self.ws = websocket.WebSocketApp(
            self.host,
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open
        )
        
        self.mq = MessageManager()
        
        # Setup inbound communication
        self.mq.set_consumer('coordinator')
        self.mq.init_consumer()
        self.mq.register_callback(self.handle_message)
        self.mq.add_queue('coordinator')
        self.mq.consume()
        
        # Then outbound
        self.mq.set_producer('slave')
        self.mq.init_producer()
    
    def run(self):
        jobs = [
            gevent.spawn(self.mq.run),
            gevent.spawn(self.ws.run_forever),
        ]
        try:
            gevent.joinall(jobs)
        except KeyboardInterrupt:
            self.close()
    
    def on_open(self, ws):
        print 'Connected to the Load Balancer!'
    
    def on_close(self, ws):
        print 'Connection was closed.'
        self.close()
    
    def on_error(self, ws, error):
        print error
    
    # Load Balancer Handling
    def on_message(self, ws, message):
        "Handle a message from the Load Balancer"
        if not message:
            return
        
        message = json.loads(message)
        if message['action'] == ACKNOWLEDGE:
            pass
        elif message['action'] == NEW_CONNECTION:
            self.r_new_connection(message['client_id'])
    
    def send_message(self, message):
        "Send a message to the Load Balancer"
        try:
            self.ws.send(json.dumps(message))
        except WebSocketError, e:
            print e
    
    def r_new_connection(self, client_id):
        "There was a new connection, send them to the default node."
        print 'Moving {0} to {1}'.format(client_id, DEFAULT_NODE)
        self.r_jump_request(client_id, DEFAULT_NODE)
    
    def lb_move_connection(self, client_id, location):
        "Tell the Load Balancer to move a connection"
        print 'Moving {0} to {1}'.format(client_id, location)
        message = {
            'action': MOVE_CONNECTION,
            'client_id': client_id,
            'host': location,
        }
        self.send_message(message)
    
    # Message Queue Handling
    def handle_message(self, message):
        "Handle a message from the Message Queue"
        print 'Recv: {0}'.format(message)
        action = message['action']
        if action == ACKNOWLEDGE:
            if message['msg_id'] == NEW_NODE:
                self.ack_new_node(
                    message['location'],
                    message['node_id'],
                    message['addr']
                )
                return True
            
            elif message['msg_id'] == PLAN_CONNECTION:
                self.ack_plan_connection(message['client_id'], message['location'])
                return True
        
        elif action == NEW_NODE:
            new_message = {
                'action': ACKNOWLEDGE,
                'msg_id': NEW_NODE,
                'node_id': message['node_id'],
            }
            self.mq.send_producer(new_message, routing_key=message['node_id'])
            return True
        
        elif action == JUMP_REQUEST:
            self.r_jump_request(
                message['client_id'],
                message['location'],
                node_id=message['node_id']
            )
            return True
        
        elif action == REMOVE_NODE:
            self.r_remove_node(message['node_id'], message['location'])
            return True
        
        elif action == LIST_LOCATIONS:
            self.r_list_locations(message['node_id'], message['client_id'])
            return True
        
        return False
    
    def s_new_node(self, location, data, waitlist=[]):
        "Request a node to simulate the location"
        self.destinations[location] = {
            'node_id': '',
            'waitlist': waitlist,
        }
        
        message = {
            'action': NEW_NODE,
            'location': location,
            'data': data,
        }
        self.mq.send_producer(message)
    
    def ack_new_node(self, location, node_id, addr):
        "Handle the acknowledgement of a node"
        
        # If the location doesn't exist, tell the node to stop and wait
        # for new instructions.
        if location not in self.destinations:
            self.s_remove_node(node_id, location)
            return
        
        self.destinations[location]['node_id'] = node_id
        self.nodes[node_id] = addr
        
        for client_id in self.destinations[location]['waitlist']:
            print 'Sending {0} to {1}.'.format(client_id, location)
            self.s_plan_connection(client_id, {}, location)
    
    def s_plan_connection(self, client_id, data, location):
        "Tell the node of 'location' to prepare for 'client_id'"
        node_id = self.destinations[location]['node_id']
        message = {
            'action': PLAN_CONNECTION,
            'node_id': node_id,
            # 'location': location,
            'client_id': client_id,
            'data': data,
        }
        self.mq.send_producer(message, routing_key=node_id)
    
    def ack_plan_connection(self, client_id, location):
        "Now that the node is prepared, move the connection."
        destination = self.destinations[location]
        host = self.nodes[destination['node_id']]
        self.lb_move_connection(client_id, host)
    
    def s_remove_node(self, node_id, location):
        "Tells a node to stop it's current job."
        message = {
            'action': REMOVE_NODE,
            'node_id': node_id,
            'location': location,
        }
        self.mq.send_producer(message, routing_key=node_id)
    
    def r_remove_node(self, node_id, location):
        "A Node requested to be removed. We will acknowledge that request."
        if location in self.destinations:
            del self.destinations[location]
        if node_id in self.nodes:
            del self.nodes[node_id]
        
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': REMOVE_NODE,
            'node_id': node_id,
        }
        self.mq.send_producer(message, routing_key=node_id)
    
    def r_jump_request(self, client_id, location, node_id=None):
        "Send the user to the proper location, and start up a node if needed"
        # If no location is specified, send them to the default node.
        if not location:
            location = DEFAULT_NODE
        
        # Check if there is a node with the location already
        if location not in self.destinations:
            # Check if location actually exists
            if location not in self.locations:
                message = '{0} tried to jump to {1} which does not exist.'
                print message.format(client_id, location)
                self.s_ack_jump_request(client_id, False, node_id)
                
            else:
                # Start up a new node for the location and put the client
                # into a waitlist.
                self.s_ack_jump_request(client_id, True, node_id)
                data = self.locations[location]
                self.s_new_node(location, data, waitlist=[client_id])
        
        else:
            self.s_ack_jump_request(client_id, True, node_id)
            
            # If there is a node, check if it's fully loaded.
            destination = self.destinations[location]
            if not destination['node_id']:
                destination['waitlist'].append(client_id)
            
            else:
                self.s_plan_connection(client_id, {}, location)
    
    def s_ack_jump_request(self, client_id, accepted, node_id):
        "Send an acknowledgement that the jump request was accepted or denied"
        if not node_id:
            return
        
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': JUMP_REQUEST,
            'client_id': client_id,
            'accepted': accepted,
        }
        self.mq.send_producer(message, routing_key=node_id)
    
    def r_list_locations(self, node_id, client_id):
        "Tell the Node what the list of locations are."
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': LIST_LOCATIONS,
            'node_id': node_id,
            'client_id': client_id,
            'locations': self.locations.keys(),
        }
        self.mq.send_producer(message, routing_key=node_id)
    
    def close(self):
        "Stop the Coordinator"
        self.running = False
        if self.mq:
            self.mq.close()
        
        if self.ws:
            self.ws.close()

if __name__ == "__main__":
    client = Coordinator(COORDINATOR)
    client.run()

MESSAGES_NAMES = {
    0: 'OFFLINE',
    1: 'READY',
    2: 'ACTIVE',
    11: 'ACKNOWLEDGE',
    12: 'PING',
    13: 'PONG',
    51: 'NEW_CONNECTION',
    52: 'MOVE_CONNECTION',
    53: 'LOST_CONNECTION',
    54: 'NEW_NODE',
    55: 'REMOVE_NODE',
    56: 'WHO_ARE_YOU',
    57: 'PROXY_ID',
    58: 'PLAN_CONNECTION',
    59: 'LIST_NODES',
    60: 'LIST_LOCATIONS',
    101: 'JUMP_REQUEST',
    102: 'NODE_INFO',
    103: 'JUMP_DENIED',
    104: 'UPDATE_CLIENT_INFO',
    105: 'SAVE_CLIENT',
    151: 'MSG_SYSTEM',
    152: 'MSG_GLOBAL',
    153: 'MSG_PLAYER',
    154: 'MSG_COMMAND',
    155: 'WHO_AM_I',
}

# 1-50: General Messages
ACKNOWLEDGE = 11
PING = 12
PONG = 13

# 51 - 99: Coordinator Message IDs (Coordinator > Node|Service)
NEW_CONNECTION = 51 # Tell the TC there was a new connection.
MOVE_CONNECTION = 52 # Tell the LB to move the user to another server.
LOST_CONNECTION = 53 # Tell the TC a client has disconnected.
NEW_NODE = 54 # Tell the LB a node has been connected.
REMOVE_NODE = 55 # Tell the LB a node has been removed.
WHO_ARE_YOU = 56 # Ask a Node who they are.
PROXY_ID = 57 # Update tbe Coordinator with the server connection ID.
PLAN_CONNECTION = 58 # Tell the node there will be a new connection.
LIST_NODES = 59 # Return a list of current nodes.
LIST_LOCATIONS = 60 # Return a list of locations.

# 101 - 149: Node Message IDs (Node > Coordinator|Service)
JUMP_REQUEST = 101 # There is a jump request from the node.
NODE_INFO = 102 # Request node information
UPDATE_CLIENT_INFO = 104 # Update the client information.
SAVE_CLIENT = 105 # Save the client data.

# 151 - 199: Client Message IDs (Client > Node)
MSG_SYSTEM = 151
MSG_GLOBAL = 152
MSG_PLAYER = 153
MSG_COMMAND = 154
WHO_AM_I = 155


# CONSTANTS
OFFLINE = 0
READY = 1
ACTIVE = 2

# TODO: Replace str keys in messages with the variables below in order to
# reduce bandwidth, especially when using msgpack instead of JSON.
ACTION = 50
DATA = 51
MSG_ID = 52

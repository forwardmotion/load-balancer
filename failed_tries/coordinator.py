import socket
import select
import json

# Constants
from message_ids import *

from client import Client

HOST = '127.0.0.1'
# 22000 - 22100 for services
COORDINATOR = ('', 22002) # 22002 - Coordinator Listening Port for nodes
LOAD_BALANCER = ('', 22001) # 22001 - Load Balancer Coordinator Port

class Coordinator(object):
    """
    The Coordinator is what determines which users connect to which systems or
    nodes in the game. If they aren't logged in, tell the Game Proxy to connect
    the user to the Game Login server.

    Manages all the nodes and users in the game.
    """
    
    def __init__(self):
        self.load_balancer = None
        self.server = None
        self.system_nodes = {} # node_id: node
        self.node_ids = {} # node: node_id
        self.clients = {}
        
        self.default_node = None
        
        # Connect to the Proxy
        self.load_balancer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.load_balancer.connect(LOAD_BALANCER)
        
        # Begin accepting connections from system nodes
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(COORDINATOR)
        self.server.listen(5)
        
        self.running = False
        self.inputs = []
        
        self.data_size = 1024
        
    def run(self):
        self.running = True
        inputs = self.inputs
        servers = [self.server, self.load_balancer]
        system_nodes = self.system_nodes
        
        while self.running:
            inputs = servers + self.system_nodes.values()
            idata, odata, edata = select.select(inputs, [], [])
            
            for data in idata:
                if data == self.server:
                    # A new node has connected to the Coordinator.
                    self.handle_new_node(data)
                
                elif data == self.load_balancer:
                    # There was a new connection on the Load Balancer.
                    self.handle_coordination(data)
                
                elif data in system_nodes:
                    # A node has a request of some sort.
                    self.handle_node(data)

    def handle_new_node(self, server):
        "Accept a connection from a node server."
        node, node_id = server.accept()
        self.system_nodes[node_id] = node
        message = {'action': WHO_ARE_YOU}
        message = json.dumps(message)
        node.send(message)
    
    def handle_coordination(self, server):
        "Handle requests from the Load Balancer"
        data = server.recv(self.data_size)
        data = json.loads(data)
        
        if data['action'] == NEW_CONNECTION:
            # Handle a new connection to the Game Cluster.
            client_id = data['client_id']
            
            client = Client()
            client.client_id = client_id
            client.node_id = self.default_node
            
            message = {
                'action': MOVE_CONNECTION,
                'node_id': self.default_node,
                'client_id': client_id,
            }
            message = json.dumps(message)
            self.load_balancer.send(message)
        
        elif data['action'] == LOST_CONNECTION:
            client_id = data['client_id']
    
    def handle_node(self, node):
        "Handle requests from the nodes."
        data = node.recv(self.data_size)
        data = json.loads(data)
        
        if data['action'] == JUMP_REQUEST:
            # Handle a request to jump to a new system.
            pass
        
        elif data['action'] == UPDATE_CLIENT_INFO:
            pass
        
        elif data['action'] == SAVE_CLIENT:
            pass

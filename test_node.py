from gevent import monkey
monkey.patch_all()

from MessageManager import MessageManager

import gevent
from gevent.queue import Empty as QueueEmpty
from geventwebsocket import WebSocketServer, WebSocketApplication, Resource, WebSocketError
from multiprocessing import Process, Queue #, Empty as QueueEmpty

import sys
import json
import uuid
from collections import OrderedDict

from message_ids import *

DEBUG = True
def debug(string):
    if DEBUG:
        print string

class Client(WebSocketApplication):
    server = None
    
    # Receiving Messages
    def on_message(self, message):
        if not message:
            return
        
        if message == 'ping':
            self.ws.send('pong')
        else:
            message = json.loads(message)
            action = message['action']
            
            if action == WHO_AM_I:
                self.r_who_am_i()
            elif action == JUMP_REQUEST:
                self.r_jump_request(message['location'])
            elif action == LIST_LOCATIONS:
                self.r_list_locations()
            elif action == MSG_SYSTEM:
                self.r_msg_system(message['message'])
            elif action == NODE_INFO:
                self.r_node_info()
            else:
                debug('[{0}][{1}]: {2}'.format(
                    self.client_id,
                    MESSAGES_NAMES[action],
                    message
                ))
    
    def r_who_am_i(self):
        debug('[{0}->NODE][WHO_AM_I]'.format(self.client_id))
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': WHO_AM_I,
            'data': 'You are {0} at {1} on the node {2}.'.format(
                self.client_id,
                self.server.location,
                self.server.server_id,
            ),
        }
        self.send_message(message)
    
    def r_jump_request(self, location):
        debug('[{0}][JUMP_REQUEST]: {1}'.format(self.client_id, location))
        self.server.cr_jump_request(self.client_id, location)
    
    def r_list_locations(self):
        self.server.cr_list_locations(self.client_id)
    
    def r_msg_system(self, message):
        self.server.cr_msg_system(self.client_id, message)
    
    def r_node_info(self):
        self.server.cr_node_info(self.client_id)
    
    # Commands from the server
    def set_data(self, data):
        self.data = data
    
    def send_message(self, message):
        action = message['action']
        if action == ACKNOWLEDGE:
            debug('[NODE->{0}][ACKNOWLEDGE][{1}]'.format(
                self.client_id,
                MESSAGES_NAMES[message['msg_id']]
            ))
        else:
            pass
            # debug('[NODE][{0}]: {1}'.format(
            #     MESSAGES_NAMES[message['action']],
            #     message
            # ))
        
        if not self.ws.closed:
            try:
                self.ws.send(json.dumps(message))
            except WebSocketError, e:
                print e
    
    def close(self):
        self.ws.close()
    
    # Events
    def on_open(self):
        self.client_id = self.ws.environ['HTTP_CONNECTION_ID']
        debug(str(self.client_id) + ' connected!')
        if self.server:
            self.server.new_connection(self.client_id, self)
    
    def on_close(self, reason):
        debug(str(self.client_id) + ' disconnected!')
        self.server.lost_connection(self.client_id)


class Server(Process):
    
    def __init__(self, host):
        Process.__init__(self)
        Client.server = self
        
        self.host = host
        self.server_id = str(uuid.uuid4())
        self.location = ''
        
        self.clients = {}
        self.planned_clients = {}
        
        self.state = OFFLINE
        
        self.data = {}
        self.running = False
        
        self.start_services()
    
    def start_services(self):
        self.server = WebSocketServer(
            self.host,
            Resource(
                OrderedDict({
                    '/': Client,
                })
            )
        )
        
        self.mq = MessageManager()
        
        # Setup inbound communication
        self.mq.set_consumer('slave')
        self.mq.init_consumer()
        self.mq.register_callback(self.handle_message)
        self.mq.add_queue(self.server_id)
        self.mq.add_queue('all')
        self.mq.consume()
        
        # Then outbound
        self.mq.set_producer('coordinator')
        self.mq.init_producer()
        
        # Send a test message
        debug('[NODE->COORD][NEW_NODE]: {0}'.format(self.server_id))
        self.mq.send_producer({
            'action': NEW_NODE,
            'node_id': self.server_id,
        })
    
    def run(self):
        self.running = True
        jobs = [
            gevent.spawn(self.server.serve_forever),
            gevent.spawn(self.mq.run),
        ]
        try:
            gevent.joinall(jobs)
        except (KeyboardInterrupt):
            self.stop()
    
    def stop(self):
        "Tell the server to shutdown."
        self.restart()
        self.running = False
    
    def restart(self):
        "Shutdown and then prepare for reassignment"
        for client_id in self.clients:
            self.clients[client_id].close()
        
        self.state = OFFLINE
        self.location = ''
        self.data = {}
        
        if not self.running:
            self.mq.close()
            self.server.stop()
    
    def new_connection(self, client_id, client):
        self.state = ACTIVE
        
        self.clients[client_id] = client
        
        # If the client is in the list of planned clients, set the data for
        # the client.
        if client_id in self.planned_clients:
            client.set_data(self.planned_clients[client_id])
            del self.planned_clients[client_id]
            
            message = {
                'action': ACKNOWLEDGE,
                'msg_id': NEW_CONNECTION,
                'location': self.location,
            }
            client.send_message(message)
        
    def lost_connection(self, client_id):
        del self.clients[client_id]
        if len(self.clients) == 0 and len(self.planned_clients) == 0:
            self.s_remove_node()
    
    # Coordinator Messages
    def handle_message(self, message):
        if type(message) == unicode or type(message) == str:
            return True
        
        if 'node_id' in message:
            if message['node_id'] != self.server_id:
                return False
        
        print '[COORD][{0}]: {1}'.format(
            MESSAGES_NAMES[message['action']],
            message
        )
        
        action = message['action']
        if action == NEW_NODE and self.state == OFFLINE:
            self.r_new_node(message['location'], message['data'])
            return True
        
        elif action == PLAN_CONNECTION:
            self.r_plan_connection(message['client_id'], message['data'])
            return True
        
        elif action == ACKNOWLEDGE:
            msg_id = message['msg_id']
            if msg_id == JUMP_REQUEST:
                self.ack_jump_request(message['client_id'], message['accepted'])
            elif msg_id == REMOVE_NODE:
                self.ack_remove_node()
            elif msg_id == LIST_LOCATIONS:
                self.ack_list_locations(message['client_id'], message['locations'])
            return True
        
        elif action == REMOVE_NODE:
            self.state = OFFLINE
            return True
        
        return False
    
    def r_new_node(self, location, data):
        "Start up the node to be 'location' with 'data'"
        self.state = READY
        self.data = data
        self.location = location
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': NEW_NODE,
            'location': location,
            'node_id': self.server_id,
            'addr': 'ws://{0}:{1}/'.format(
                self.server.server_host,
                self.server.server_port
            ),
        }
        debug('[NODE->COORD][ACK][NEW_NODE]: {0}'.format(location))
        self.mq.send_producer(message)
    
    def r_plan_connection(self, client_id, data):
        self.planned_clients[client_id] = data
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': PLAN_CONNECTION,
            'client_id': client_id,
            'location': self.location,
        }
        debug('[NODE->COORD][ACK][PLAN_CONNECTION]: {0}'.format(client_id))
        self.mq.send_producer(message)
    
    def s_remove_node(self):
        message = {
            'action': REMOVE_NODE,
            'node_id': self.server_id,
            'location': self.location,
        }
        debug('[NODE->COORD][REMOVE_NODE]: {0}'.format(self.location))
        self.mq.send_producer(message)
    
    def ack_remove_node(self):
        "Prepare the node to be used again"
        self.restart()
    
    def ack_list_locations(self, client_id, locations):
        "Received a list from the Coordinator, now send it to the Client."
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': LIST_LOCATIONS,
            'locations': locations,
        }
        self.send_client(client_id, message)
    
    def ack_jump_request(self, client_id, accepted):
        "Disconnect the client so if the system is empty we can be reused."
        if accepted and client_id in self.clients:
            self.clients[client_id].close()
    
    # Client Messages
    def cr_jump_request(self, client_id, location):
        "Client made a jump request."
        message = {
            'action': JUMP_REQUEST,
            'node_id': self.server_id,
            'client_id': client_id,
            'location': location,
            'data': self.clients[client_id].data,
        }
        debug('[NODE->COORD][JUMP_REQUEST]: {0} to {1}'.format(client_id, location))
        self.mq.send_producer(message)
    
    def cr_list_locations(self, client_id):
        "Client requested a list of locations."
        message = {
            'action': LIST_LOCATIONS,
            'node_id': self.server_id,
            'client_id': client_id,
        }
        debug('[NODE->COORD][LIST_LOCATIONS]')
        self.mq.send_producer(message)
    
    def cr_msg_system(self, client_id, string):
        "Client is sending a message to the system."
        message = {
            'action': MSG_SYSTEM,
            'client_id': client_id,
            'message': string,
        }
        debug('[{0}][MSG_SYSTEM]: {1}'.format(client_id, string))
        self.send_all(message)
    
    def cr_node_info(self, client_id):
        "Client is requesting information on the node/location."
        message = {
            'action': ACKNOWLEDGE,
            'msg_id': NODE_INFO,
            'data': self.data,
        }
        self.send_client(client_id, message)
    
    def send_client(self, client_id, message):
        self.clients[client_id].send_message(message)
    
    def send_all(self, message):
        "Send a message to all the Clients"
        clients = self.clients
        for client_id in clients:
            clients[client_id].send_message(message)

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        server = Server(('127.0.0.1', int(sys.argv[1])))
    else:
        server = Server(('127.0.0.1', 22100))
    server.run()

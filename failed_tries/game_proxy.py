import json
import socket

# Constants
from message_ids import *

from datetime import datetime
import logging
LOG_FILENAME = 'logs/' + datetime.now().strftime('%d-%m-%Y_balancer.log')
logging.basicConfig(
    filename=LOG_FILENAME,
    format='%(asctime)s:%(levelname)s: %(message)s',
    level=logging.DEBUG
)

from load_balancer import BalanceServer
# 22001 Load Balancer Listening port for the coordinator
# 22000 Load Balancer Listening Port for clients
COORDINATOR = ('', 22001)
HOST = ('', 22000)


class GameProxy(BalanceServer):
    
    def __init__(self, host, coordinator):
        BalanceServer.__init__(self, host)
        
        self.coordinator = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.coordinator.bind(coordinator)
        self.coordinator.listen(5)
        self.servers.append(self.coordinator)
    
    def get_next_node(self, client_id):
        # If we don't have a coordinator connected, then use the predefined
        # nodes.
        if self.coordinator:
            # Tell Coordinator we have a new connection
            message = {'action': 'new_connection', 'client_id': client_id}
            message = json.dumps(message)
            self.coordinator.send(message)
            return None
        else:
            return BalanceServer.get_next_node(self)
    
    def coordinator_check(self, coordinator):
        "Does the Coordinator have new instructions for us?"
        coordinator_data = coordinator.recv(self.data_size)
        message = json.loads(coordinator_data)
        if message['action'] == MOVE_CONNECTION:
            node_id = message['node_id']
            client_id = message['client_id']
            self.move_connection(client_id, node_id)
        
        elif message['action'] == NEW_NODE:
            node_id = message['node_id']
            node_ip = message['node_ip']
            self.add_node(node_id, node_ip)
        
        elif message['action'] == REMOVE_NODE:
            node_id = message['node_id']
            self.remove_node(node_id)
    
    def add_node(self, node_id, node_ip):
        "Add a node to the usability list."
        self.nodes[node_id] = message['node_ip']
        message = "{0} has been added to the node list."
        logging.info(message)
    
    def remove_node(self, node_id):
        "Remove a node from the usability list."
        del self.nodes[node_id]
        message = "{0} has been removed from the node list."
        logging.info(message)
    
    def move_connection(self, client_id, new_node_id):
        "This moves a connection from one node to another."
        connection = self.connections[client_id]
        # Close old connection
        server_socket = connection[1]
        if server_socket:
            server_socket.send('');
            server_socket.close();
        
        # Set up new connection
        source = self.get_source_ip(client_id)
        try:
            server_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_connection.bind((source, 0)) 
        except Exception, e:
            logging.critical(str(e) + ' ' + sys.exc_info()[0])
            try:
                server_connection.bind((source, 0))
            except:
                logging.critical(str(e))
        
        server_connection.connect(nodes[node])
        server_ids[client_id] = server_connection
        connection[1] = server_connection
        
        message = "{0} moved to node {1}".format(client_id, node_id)
        logging.info(message)
    
    def new_data(self, data):
        "Called when an input hasn't been processed."
        if data == self.coordinator:
            self.coordinator_check(data)
        

if __name__ == '__main__':
    server = GameProxy(HOST, COORDINATOR)
    server.run()

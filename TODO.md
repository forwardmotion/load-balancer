TODO
==

Here are a list of things that need to be done to improve the setup.

- Make Coordinator Stateless
    - Make Nodes dynamically listen to the routing_key of their location (ie. 'default')
    - Use MemcacheD for storing Client state?
- Make the Load Balancer use the Message Queue?
- Better logging
- Have inactive Nodes listen on the routing_key 'slave', but stop listening when active.

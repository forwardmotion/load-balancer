The Load Balancer
==
The Load Balancer handles connections from the outside world and proxies them
to the appropriate server. The server that the client is proxied to depends on
where The Coordinator tells the Load Balancer to connect them to. The servers on Forward Motion
include a System Node, Login Node, or Planet Node.

Prerequisites
==
1. Python 2.7
2. RabbitMQ
3. `pip install gevent`
4. `pip install gevent-websocket`
5. `pip install websocket-client`
6. `pip install kombu`
7. `pip install librabbitmq` -- For performance

Example Setup
==
1. Run `ws_balancer.py` -- Load Balancer
2. Run `test_coordinator.py` -- Coordinator
3. Run `test_node.py` -- Node (one or more times)
4. Run `test_client.py` -- Client

After step 4, you can look at the output for `ws_balancer.py`, `test_coordinator.py`, and `test_node.py`. All should show output for the newly connected user.

In the `test_client.py`, hit `ENTER` without any input to wait for a message (CTRL+C to cancel). Then type `whoami` to send a `WHO_AM_I` request. Hit `ENTER` to listen for the response. You can also type `jump 1` or `2` or `default` to switch the place you want to go to. If you have two nodes, your Client will be reconnected to a new node, or if you only have one node running, it will reconnect you to that same node, but the node will be re-identified as the new location you chose to "jump" to.

When you first start up `test_node.py` you will notice the first line is `[NODE->COORD][NEW_NODE]: ...`. This is the node sending a test message to the Coordinator. A second line should appear `[COORD][ACKNOWLEDGE]` if the Coordinator is listening for messages.

When a Client connects, a lot more happens. First the Load Balancer sends the Coordinator a message saying there is a new connection. The Coordinator then wants to send that Client to the default location. So it will check it's local list of nodes that are currently being hosted. If the default location isn't being hosted it sends a `NEW_NODE` request to the RabbitMQ server saying it wants a new node for the default location. If a node is available, it will set itself up to be that location then sends an `ACKNOWLEDGE` message back with the information needed to connect to it. The Coordinator then records the node and response information and then tells the Load Balancer to connect the Client to that Node. This allows Nodes to be hosted on separate machines if necessary and allows the entire system to scale.

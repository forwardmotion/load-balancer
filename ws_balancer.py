from gevent import monkey
monkey.patch_all()

import json
import uuid
from collections import OrderedDict

import gevent
from gevent.queue import Empty as QueueEmpty
from geventwebsocket import WebSocketServer, WebSocketApplication, Resource, WebSocketError

import websocket
websocket.enableTrace(True)

from multiprocessing import Process, Queue #, Empty as QueueEmpty

from message_ids import *

DEBUG = True
def debug(*args):
    if DEBUG:
        print args

# 22000 - 22100 for services
COORDINATOR = ('127.0.0.1', 22002) # 22002 - Coordinator Listening Port
LOAD_BALANCER = ('', 22001) # 22001 - Load Balancer Listener for Clients


class ExternalConnector(WebSocketApplication):
    "This is the WebSocket handler for external connections."
    
    def on_open(self):
        self.client = Client(self)
        debug(str(self.client.client_id) + '-EX connected!')
    
    def on_message(self, message):
        if not message:
            return
        
        self.client.external_internal(message)
    
    def send_message(self, message):
        try:
            self.ws.send(message)
        except WebSocketError, e:
            print e
    
    def on_close(self, *args):
        debug(str(self.client.client_id) + '-EX disconnected!')
        print args
        self.client.lost_connection()

class InternalConnector(object):
    "This is the WebSocket client that connects to different nodes."
    
    def __init__(self, client, host):
        self.client = client
        self.ws = websocket.WebSocketApp(
            host,
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open,
            header={'CONNECTION_ID': self.client.client_id}
        )
        
        self.connected = False
    
    def run(self):
        # Theoretically websocket-client should be monkey-patched.
        debug("{0}-IN running!".format(self.client.client_id))
        self.ws.run_forever()
    
    def on_open(self, ws):
        debug(str(self.client.client_id) + '-IN connected!')
        self.connected = True
    
    def on_message(self, ws, message):
        if not message:
            return
        
        self.client.internal_external(message)
    
    def send_message(self, message):
        if self.connected:
            try:
                self.ws.send(message)
            except WebSocketError, e:
                print e
    
    def on_close(self, ws, *args):
        debug(str(self.client.client_id) + '-IN disconnected!')
        debug(str(args))
        self.client.internal_closed()
    
    def on_error(self, ws, error):
        print error
    
    def close(self):
        self.ws.close()


class Client(object):
    server = None
    
    def __init__(self, external):
        self.client_id = str(uuid.uuid4())
        self.external = external
        self.internal = None
        self.next_host = ""
        
        if self.server:
            self.server.new_connection(self.client_id, self)
    
    def internal_external(self, message):
        "Pass the message from the node to the client."
        if self.external:
            self.external.send_message(message)
    
    def external_internal(self, message):
        "Pass the message from the client to the node."
        if self.internal:
            print message
            self.internal.send_message(message)
    
    def new_connection(self, host):
        "Close old connection if there is one, then connect to a new node."
        self.next_host = host
        if self.internal:
            self.internal.close()
        else:
            self.make_connection()
    
    def lost_node(self):
        self.server.lost_node(self.client_id)
    
    def lost_connection(self):
        if self.internal:
            self.internal.close()
        
        self.server.lost_connection(self.client_id)
    
    def internal_closed(self):
        "Delete the old connector and then make the new one"
        del self.internal
        self.internal = None
        self.make_connection()
    
    def make_connection(self):
        "Make a new connection if there is a new host to connect to."
        if self.next_host:
            self.internal = InternalConnector(self, self.next_host)
            self.next_host = None
            gevent.spawn(self.internal.run)


class Coordinator(WebSocketApplication):
    server = None
    
    def on_open(self):
        self.server.new_coordinator(self)
        debug('The Coordinator has connected!')
    
    def on_message(self, message):
        if not message:
            return
        
        message = json.loads(message)
        if message['action'] == MOVE_CONNECTION:
            self.r_move_connection(message['client_id'], message['host'])
    
    def r_move_connection(self, client_id, host):
        if client_id not in self.server.clients:
            print '{0} is not connected.'.format(client_id)
            return
        
        print '{0} is being reconnected to {1}'.format(client_id, host)
        self.server.clients[client_id].new_connection(host)
    
    def s_new_connection(self, client_id):
        message = {
            'action': NEW_CONNECTION,
            'client_id': client_id,
        }
        self.send_message(message)
    
    def s_lost_connection(self, client_id):
        message = {
            'action': LOST_CONNECTION,
            'client_id': client_id,
        }
        self.send_message(message)
    
    def send_message(self, message):
        if not self.ws.closed:
            try:
                self.ws.send(json.dumps(message))
            except WebSocketError, e:
                print e
    
    def on_close(self, reason):
        debug('The Coordinator has disconnected!')


class Server(Process):
    
    def __init__(self, host, coordinator):
        Process.__init__(self)
        Client.server = self
        Coordinator.server = self
        
        self.server = WebSocketServer(
            host,
            Resource(
                OrderedDict({
                    '/': ExternalConnector,
                })
            )
        )
        
        self.ws_coordinator = WebSocketServer(
            coordinator,
            Resource(
                OrderedDict({
                    '/': Coordinator,
                })
            )
        )
        
        self.coordinator = None
        
        self.clients = {}
        self.nodes = {}
        self.default_node = None
        
        self.running = False
    
    def run(self):
        self.running = True
        jobs = [
            gevent.spawn(self.server.serve_forever),
            gevent.spawn(self.ws_coordinator.serve_forever),
        ]
        try:
            gevent.joinall(jobs)
        except KeyboardInterrupt:
            self.stop()
    
    def stop(self):
        self.server.stop()
        self.running = False
    
    def new_connection(self, client_id, client):
        self.clients[client_id] = client
        print '{0} connected.'.format(client_id)
        
        if self.coordinator:
            self.coordinator.s_new_connection(client_id)
    
    def lost_connection(self, client_id):
        del self.clients[client_id]
        
        if self.coordinator:
            self.coordinator.s_lost_connection(client_id)
    
    def new_coordinator(self, coordinator):
        """
        When there is a new coordinator and we already have connected clients
        tell the coordinator about them.
        """
        self.coordinator = coordinator
        for client_id in self.clients:
            self.coordinator.s_new_connection(client_id)


if __name__ == '__main__':
    server = Server(LOAD_BALANCER, COORDINATOR)
    server.run()
    

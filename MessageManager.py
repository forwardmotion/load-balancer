import kombu
import gevent
from socket import error as socket_error

class MessageManager(object):
    
    def __init__(self):
        self.connection = kombu.Connection()
        self.connection.connect()
        
        self.producer = None
        self.producer_ex = None
        self.on_return = None
        
        self.consumer = None
        self.consumer_name = ""
        self.consumer_ex = None
        self.callback = None
        
    # Consumer Stuff
    def set_consumer(self, name, ex_type='direct'):
        self.consumer_ex = kombu.Exchange(name, type=ex_type)
        self.consumer_name = name
    
    def set_producer(self, name, ex_type='direct'):
        self.producer_ex = kombu.Exchange(name, type=ex_type)
        self.producer_name = name
        
    def add_queue(self, routing_key):
        queue = kombu.Queue(
            self.consumer_name,
            self.consumer_ex,
            routing_key=routing_key
        )
        self.consumer.add_queue(queue)
    
    def init_consumer(self, accept=['json']):
        self.consumer = kombu.Consumer(
            self.connection,
            accept=accept
        )
        self.consumer.register_callback(self.handle_message)
    
    def register_callback(self, callback):
        self.callback = callback
    
    def consume(self):
        self.consumer.consume()
    
    def handle_message(self, body, message):
        if self.callback(body):
            message.ack()
        else:
            message.reject(requeue=True)
    
    # Producer Stuff
    def init_producer(self):
        self.producer = kombu.Producer(
            self.connection,
            exchange=self.producer_ex,
            on_return=self.on_return
        )
    
    def send_producer(self, message, routing_key=None):
        routing_key = routing_key if routing_key else self.producer_name
        self.producer.publish(
            message,
            serializer='json',
            routing_key=routing_key,
            delivery_mode=1, # Transient
            expiration=1
        )
    
    # General
    def drain_events(self, timeout=0.2):
        try:
            self.connection.drain_events(timeout=timeout)
        except socket_error:
            return
    
    def run(self):
        self.running = True
        while self.running:
            self.drain_events()
    
    def close(self):
        self.running = False
        gevent.sleep(0.4)
        self.connection.release()
    
